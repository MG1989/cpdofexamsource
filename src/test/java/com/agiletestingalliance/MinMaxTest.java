package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest 
{
	@Test
	public void testFindMinMaxValOne()
	{
		int result = new MinMax().findMinMaxVal(2, 1);
		assertEquals("MinMax", 2, result);	
	}
	
	@Test
	public void testFindMinMaxValTwo()
	{
		int result = new MinMax().findMinMaxVal(1, 2);
		assertEquals("MinMax", 2, result);
	}

}

