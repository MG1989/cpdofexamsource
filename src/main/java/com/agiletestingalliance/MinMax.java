package com.agiletestingalliance;

public class MinMax {

    public int findMinMaxVal(int firstVar, int secondVar) {
        if (secondVar > firstVar)
        {
            return secondVar;
        }
        else
        {
        	return firstVar;
        }
    }

}
